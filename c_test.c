#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#define HEAD_SEQ_LEN 4
#define HEAD_DAT_LEN 2
#define HEAD_LEN (HEAD_SEQ_LEN+HEAD_DAT_LEN)

typedef struct data {
    unsigned int seq_num;
    unsigned short len;
    unsigned char *seg_data;
}SegData;

typedef struct node {
    SegData *data_info;
    struct node *left;
    struct node *right;
}Node;

/* Given a binary tree, print its nodes in inorder*/
void printInorder(Node *node, FILE *fp_data) {
    if (node == NULL)
        return;
 
    /* first recur on left child */
    printInorder(node->left, fp_data);
 
    /* then print the data of node */
    //printf("%d\n", node->data_info->seq_num);
    fwrite(node->data_info->seg_data, node->data_info->len, 1, fp_data);
    free(node->data_info->seg_data);

    /* now recur on right child */
    printInorder(node->right, fp_data);
}

struct node *insertNode(Node *node, SegData *data) {
    if (node != NULL) {
        if (data->seq_num >= node->data_info->seq_num) {
            node->right = insertNode(node->right, data);
        } else {
            node->left = insertNode(node->left, data);
        }
    } else {
        node = malloc(sizeof(Node));
        node->data_info = data;
        node->left = NULL;
        node->right = NULL;
    }
    return node;
}

unsigned short buffToShort(unsigned char *buffer) {
    unsigned short a = (unsigned short)(buffer[0] << 8 | buffer[1]);
    return a;
}

unsigned int buffToInteger(unsigned char *buffer) {
    unsigned int a = (unsigned int)(buffer[0] << 24 | buffer[1] << 16 | buffer[2] << 8 | buffer[3]);
    return a;
}

int main() {
 
    int sock;
    struct sockaddr_in addr;
    unsigned char heaher[HEAD_LEN];
    unsigned char seq_n[HEAD_SEQ_LEN];
    unsigned char dat_l[HEAD_DAT_LEN];
    unsigned char *dynArr;

    unsigned int seq_num;
    unsigned short len;
    Node *root;
    int pack_cnt = 0;

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0){
        perror("TCP Socket created error");
        return -1;
    }
    printf("TCP socket created.\n");
 
    bzero(&addr,sizeof(addr));
    addr.sin_addr.s_addr = inet_addr("34.81.172.181");
    addr.sin_family = AF_INET;
    addr.sin_port = htons(49152);

    // Connect to remote server
    if (connect(sock , (struct sockaddr *)&addr , sizeof(addr)) < 0) {
        printf("Connected to the server failed.\n");
        return -1;
    }
    printf("Connected to the server.\n");

    // Start to receive packages from server
    while ( recv(sock, heaher, HEAD_LEN, MSG_WAITALL) > 0 ) {

        memcpy(seq_n, heaher, HEAD_SEQ_LEN);
        memcpy(dat_l, heaher+HEAD_SEQ_LEN, HEAD_DAT_LEN);

        seq_num = buffToInteger(seq_n);
        len = buffToShort(dat_l);
        if( seq_num < 0 || len < 0) {
            printf("Get wrong seq num or data len");
            return -1;
        }

        dynArr = malloc( len * sizeof(unsigned char) );
        recv(sock, dynArr, len, MSG_WAITALL);
        
        SegData *seg_data;
        seg_data = malloc(sizeof(SegData));

        seg_data->seq_num = seq_num;
        seg_data->len = len;
        seg_data->seg_data = dynArr;

        // Create binary tree
        if(pack_cnt == 0)
            // first node
            root = insertNode(NULL, seg_data);
        else
            insertNode(root, seg_data);
        
        pack_cnt = pack_cnt + 1;
    }

    FILE *fp_data;
    fp_data = fopen("data.bin", "wb");
    if (!fp_data) {
       fclose(fp_data);
       printf("Can not open a file to write.\n");
       return -1;
    }
    // write downloaded packages into a file
    printInorder(root, fp_data);

    fclose(fp_data);
    close(sock);

    printf("Recv pack_cnt: %d\n", pack_cnt);
    printf("Disconnected from the server.\n");
 
    return 0;
}