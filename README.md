## Getting started

* Ping `test.jigentec.com` to get ip address

  ```shell
  $ ping test.jigentec.com
  ```

![image](/image/ping.png)
 
* You can refine ip and port in code line 90 and line 92

![image](/image/ip_port.png)

## Compile and Execute

* How to compile this repo

    ```shell
    $ gcc c_test.c -o c_test
    ```

* Execute the compiled file `c_test`, and then start to download file.

    ```shell
    $ ./c_test
    ```
    
* Screenshot for result

![image](/image/exeu.png)

## SHA-256 checksum

* sha256 checksum with download file `data.bin`

    ```shell
    $ sha256sum data.bin
    ```
    
![image](/image/checksum.png)

### Reference doc.

> TCP socket related material
>> https://idiotdeveloper.com/tcp-client-server-implementation-in-c/
>>
>> https://blog.csdn.net/hhhlizhao/article/details/73912578

> binary trees related material
>> https://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/
>>
>> https://blog.onemid.net/blog/ds_binary_search_tree_recursive_insert/